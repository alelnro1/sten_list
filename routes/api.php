<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$router->group(['namespace' => 'Api'], function () use ($router) {

    $router->group(['prefix' => 'list'], function () use($router) {
        $router->get('get', 'ItemsController@getAll');
        $router->post('reorder', 'ItemsController@reorder');
    });

    $router->group(['prefix' => 'item'], function () use ($router) {
        $router->get('/{id}', 'ItemsController@get');
        $router->post('add', 'ItemsController@add');
        $router->post('add/{id}', 'ItemsController@update');
        $router->post('{id}/remove', 'ItemsController@remove');
    });
});