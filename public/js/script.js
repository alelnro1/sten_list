// Constant DOM element containing all items
const ItemsList = $('#list');
var editingItemId = null;

const Api = {
    baseURI: window.location.protocol + "//" + window.location.host + "/api",

    getItem(item_id) {
        $.ajax({
            url: this.baseURI + "/item/" + item_id,
            type: 'GET',
            dataType: 'json',
            success: (item) => {
                // Save item id to prevent malicious modifications
                editingItemId = item._id;

                // Generate a new item with data received from ws
                Drawer.showEditModal(item);
            }
        });
    },

    insertItem(item) {
        const _id = editingItemId ? editingItemId : '';

        $.ajax({
            url: this.baseURI + "/item/add/" + _id,
            type: 'POST',
            data: item,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: (data) => {
                // Generate a new item with data received from ws
                Drawer.newOrUpdateRowFromApi(data);
            }
        });
    },

    removeItem(item_id) {
        $.ajax({
            url: this.baseURI + "/item/" + item_id + "/remove",
            type: 'POST',
            contentType: false,
            processData: false,
        });
    },

    reOrder() {
        const items = ItemsList.sortable('toArray');

        $.ajax({
            url: this.baseURI + "/list/reorder",
            type: 'POST',
            data: {
                'items': items
            },
            dataType: 'json',
            success: (data) => {
                // Generate a new item with data received from ws
                Drawer.removeRow(data);
            }
        })
    },

    getList() {
        $.get(this.baseURI + "/list/get", function (data) {
            $.each(data, function (index, item) {
                Drawer.addNewRow(item);
            });
        })
    }
};

const Drawer = {
    cleanModal() {
        $('#newItemModal #edit-file').attr('src', '');
        $('#newItemModal #description').val('');
    },

    newOrUpdateRowFromApi(data) {
        console.log("hola");
        console.log(data);

        // Try to find an existing row with that ID
        const row = $('li#' + data._id);

        console.log("hola");
        console.log(data);

        if (row.length > 0) {
            this.updateRow(row, data);
        } else {
            // Append new item to item's list
            this.addNewRow(data);
        }

        // Close modal
        $('#newItemModal').modal('hide');
    },

    addNewRow(data) {
        item = new Object();
        item._id = data._id;
        item.file_path = data.file_path;
        item.description = data.description;

        ItemsList.append(Drawer.buildNewRow(item));
    },

    updateRow(row, data) {
        row.find('.description').html(data.description);
        row.find('.item-image').attr('src', data.file_path);
    },

    buildNewRow(item) {
        return (
            "<li id='" + item._id + "' class='ui-state-default'>" +
                "<div>" +
                    "<div style='float: left;'>" +
                        "<img class='item-image' src='" + item.file_path + "' width='320' height='320' /><br>" +
                        "<span class='description'>" + item.description + "</span>" +
                    "</div>" +

                    "<div style='float:right;'>" +
                        "<button type='button' class='edit-item' id='" + item._id + "'>" +
                            "<i class='fa fa-edit'></i>" +
                        "</a>" +
                        "<button type='button' class='remove-item' id='" + item._id + "'>" +
                            "<i class='fa fa-trash'></i>" +
                        "</a>" +
                    "</div>" +
                    "<div style='clear:both'></div>" +
                "</div>" +
            "</li>"
        );
    },

    removeRow(_id) {
        console.log(_id);
        $('li#' + _id).remove();
    },

    showEditModal(item) {
        $('#newItemModal #edit-file').attr('src', item.file_path);
        $('#newItemModal #description').val(item.description);

        $('#newItemModal').modal()
    }
};

// Go and fetch all the items in the database
Api.getList();

ItemsList.sortable({
    update: function(event, ui) {
        // Save new list order
        Api.reOrder();
    }
});


/**************************/
/********* EVENTS *********/
/**************************/
$('body')
    // Saving a new item
    .on('click', '.saveItem', function () {

        const formData = new FormData();
        const files = $('#file')[0].files[0];
        const description = $('#description').val();

        // Updating item
        if (editingItemId) {
            formData.append('_id', editingItemId);
        }

        formData.append('file', files);
        formData.append('description', description);

        Api.insertItem(formData);
    })

    // Remove an item
    .on('click', '.remove-item', function () {
        if (confirm('¿Remove item?')) {
            const item_id = $(this).attr('id');

            // Remove item from api
            Api.removeItem(item_id);

            // Remove item from view
            Drawer.RemoveRow(item_id);
        }
    })

    // Edit an item
    .on('click', '.edit-item', function () {
        const item_id = $(this).attr('id');

        Api.getItem(item_id)
    });

// Each time the modal hides, clean all info contained in it
$('#newItemModal').on('hidden.bs.modal', function (e) {
    Drawer.cleanModal();
});