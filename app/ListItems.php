<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model;

class ListItems extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'list_items';
    protected $primaryKey = '_id';

    protected $fillable = ['description', 'file_path', 'order'];
}
