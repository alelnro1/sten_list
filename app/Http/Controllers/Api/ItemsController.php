<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\ListItems;
use Illuminate\Http\Request;

/**
 * I create a simple items controller which manages all the the items of a list.
 * In case that more lists are needed (such as lists by id) a new ListController should be created
 *
 */
class ItemsController extends Controller
{
    /**
     * Get all items of this single list
     */
    public function getAll()
    {
        $items = ListItems::orderBy('order')->get();

        return response()->json($items);
    }

    /**
     * Get all items of this single list
     */
    public function get($item_id)
    {
        $item = ListItems::where('_id', $item_id)->firstOrFail();

        return response()->json($item);
    }

    /**
     * Create a new item and save it in DB
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        $description = $request->description;

        $file_path = $this->uploadFile($request);

        $item =
            ListItems::create([
                'description' => $description,
                'file_path' => $file_path
            ]);

        return response()->json($item);
    }

    /**
     * Update an item
     *
     * @param Request $request
     */
    public function update(Request $request)
    {
        $item = ListItems::where('_id', $request->_id)->firstOrFail();

        // If some file has been selected, upload it and save it to the item's model
        if ($request->file != "undefined"){
            $file_path = $this->uploadFile($request);

            $item->file_path = $file_path;
        }

        $item->description = $request->description;
        $item->save();

        return response()->json($item);
    }

    /**
     * Remove an item
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove($item_id, Request $request)
    {
        $item = ListItems::where('_id', $item_id)->first();

        // Remove uploaded file
        unlink($item->file_path);

        // Remove item
        $item->delete();
    }

    /**
     * Reorder the list and save each item order
     *
     * @param Request $request
     */
    public function reorder(Request $request)
    {
        // Get all items in list with no order key by id
        $items_saved = ListItems::all()->keyBy('_id');

        foreach ($request->items as $key => $item) {
            $items_saved[$item]->order = $key;
            $items_saved[$item]->save();
        }
    }

    /**
     * Upload file to "uploads" folder
     *
     * @param Request $request
     * @return bool|string
     */
    private function uploadFile(Request $request)
    {
        $directorio_destino = 'uploads/';
        $extension = $request->file->getClientOriginalExtension();
        $nombre_archivo = rand(111111, 999999) . '_' . time() . "_." . $extension;

        if ($request->file->isValid()) {
            if ($request->file->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->file->getErrorMessage();
        }

        return $url;
    }
}
